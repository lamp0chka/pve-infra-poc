# Infra PoC

Setup Proxmox VM infrastructure using Ansible by just defining VMs in a yaml file. These VM will then be [kickstarted/preseeded](https://morph027.gitlab.io/post/pve-kickseed/) for later usage with Ansible (to get their roles). This is using the [proxmox_kvm](http://docs.ansible.com/ansible/latest/proxmox_kvm_module.html) module.

**You need to adjust all vars (storage, network, ...) to some sane values according to the module options!**

## Prerequisites

### General

* passwordless SSH access to target PVE node(s)

### Python modules

* proxmoxer
* requests

## Usage

* add your target PVE node(s) to `infra` inventory file
* define your VMs in `vars.yml`
* run `ansible-playbook -i infra run.yml`

## Additional info

There is a `contrib` folder which contains kickstart, preseed, ... stuff which is neccessary to run this magic. In a true Ansible way of life one could also deploy templates of these files per vm-deployment instead of doing dynamic cmdline parsing.
